package com.example.core

import org.junit.jupiter.api.Test

class PhysicalPathModelServiceTest {
    @Test
    void testToTree(){
        def service = new AbsPhysicalPathModelService() {}
        def nodes = [
                new Area(path:'/xxx/'),
                new Area(path:'/xxx/aaa/'),
                new Area(path:'/xxx/bbb/'),
                new Area(path:'/xxx/aaa/bbb/'),
                new Area(path:'/xxx/ccc/'),
                new Area(path:'/xxx/bbb/ccc/ddd/'),
                new Area(path:'/xxx/bbb/ccc/'),
        ]
        def root = service.toTree(nodes)
        assert service.toPrettyString(root) == '''\
/xxx/
  /xxx/aaa/
      /xxx/aaa/bbb/
  /xxx/bbb/
      /xxx/bbb/ccc/
          /xxx/bbb/ccc/ddd/
  /xxx/ccc/'''
    }
}
