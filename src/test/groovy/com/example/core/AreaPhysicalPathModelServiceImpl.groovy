package com.example.core

import groovy.sql.GroovyResultSet
import groovy.sql.Sql;

/**
 * 该类仅仅提供参考实现，还有一些未完善的地方，比如事务控制。
 */
class AreaPhysicalPathModelServiceImpl implements PhysicalPathModelService<Area> {
    Sql sql = Sql.newInstance('','','','')
    /**
     * 删除节点及其后代节点
     * @param path
     * @return
     */
    Integer deleteTree(Area node) {
        sql.execute("insert into area_path(path) select path from area where path like ?",["${node.path}%"])
        sql.execute("delete from area where path like ?",["${node.path}%"])
    }

    /**
     * 根据path模糊查询
     * @param pathLike
     * @return
     */
    List<Area> queryByPathLike(String pathLike) {
        List<Area> areas = []
        sql.eachRow("select * from area where path like ?",[pathLike]){
            areas << convert(it)
        }
        areas
    }

    private Area convert(GroovyResultSet rs){
        new Area(id:rs.id,path:rs.path,name:rs.name)
    }

    /**
     * 根据path列表查询
     * @param paths
     * @return
     */
    List<Area> queryByPathIn(List<String> paths) {
        List<Area> areas = []
        sql.eachRow("select * from area where path in(${paths.collect{'?'}.join(',')})",paths){
            areas << convert(it)
        }
        areas
    }

    /**
     * 根据path模糊查询计数
     * @param pathLike
     * @return
     */
    Integer countByPathLike(String pathLike) {
        int count = 0
        sql.eachRow("select count(*) as count from area where path like ?",[pathLike]){
            count = it.count
        }
        count
    }

    /**
     * 替换path
     * @param oldPath
     * @param newPath
     * @return
     */
    Integer replacePath(Area node, String newPath) {
        sql.executeUpdate("update area set path=? where path=?",[newPath, node.path])
    }

    /**
     * 新增节点
     * @param node
     * @return
     */
    Area insert(Area node) {
        sql.executeInsert("insert into area(path,name)values(?,?)",[node.path,node.name])
    }

    /**
     * 申请节点路径
     * @param parentPath
     * @param count
     * @return
     */
    List<String> acquirePaths(String parentPath, int count) {
        List<String> paths = []
        sql.eachRow("select * from area_path where path like ? limit ?",["${parentPath}_%",count]){
            paths << it.path
        }
        if(paths){
            sql.executeUpdate("delete from area_path where path in(${paths.collect{'?'}.join(',')})",paths)
        }
        if(paths.size()<count){
            String maxPath
            sql.eachRow("select max(path) as max_path from area where path like ?",["${parentPath}_%",count]){
                maxPath = it.max_path
            }
            String beginNum = '0'*(levelPathLength()-pathPrefix().size())
            if(maxPath){
                beginNum = maxPath.split(/\//)[-2]
            }
            int from = Integer.parseInt(beginNum,36)
            ((from+1)..(from+count)).collect{
                parentPath+pathPrefix()+Integer.toString(it,36).padLeft(levelPathLength()-pathPrefix().length(),'0')
            }
        }
    }
}
