create table area(
    id primary key auto_increment comment '主键',
    path varchar(51) not null comment '节点路径',
    name varchar(64) comment '节点名称'
);

alter table area add unique index idx_area_u1(path);

create table area_path(
    id primary key auto_increment comment '主键',
    path varchar(51) not null comment '节点路径'
);
alter table area_path add unique index idx_area_path_u1(path);
