package com.example.core

interface PhysicalPathModelService<T extends PhysicalPathModel> {
    //提供一组基本操作，然后基于基本操作封装抽象逻辑

    /**
     * 删除节点及其后代节点
     * @param path
     * @return
     */
    Integer deleteTree(T node)

    /**
     * 根据path模糊查询
     * @param pathLike
     * @return
     */
    List<T> queryByPathLike(String pathLike)

    /**
     * 根据path列表查询
     * @param paths
     * @return
     */
    List<T> queryByPathIn(List<String> paths)

    /**
     * 根据path模糊查询计数
     * @param pathLike
     * @return
     */
    Integer countByPathLike(String pathLike)

    /**
     * 替换path
     * @param oldPath
     * @param newPath
     * @return
     */
    Integer replacePath(T node, String newPath)

    /**
     * 新增节点
     * @param node
     * @return
     */
    T insert(T node)

    /**
     * 申请节点路径
     * @param parentPath
     * @param count
     * @return
     */
    List<String> acquirePaths(String parentPath, int count)

    //以下包括一组抽象逻辑和默认配置
    /**
     * 是否为叶子节点
     * @param path
     * @return
     */
    default Boolean isLeafNode(String path) {
        def pathLike = "${path}%"
        countByPathLike(pathLike) <= 1
    }

    /**
     * 计算节点层级
     * @param path
     * @return
     */
    default Integer nodeLevel(String path){
        (path.length()-pathPrefix().length())/levelPathLength()
    }

    /**
     * 每一层路径长度（除根节点外）
     * @return
     */
    default Integer levelPathLength() {
        5
    }

    /**
     * 节点路径前缀
     * @return
     */
    default String pathPrefix() {
        '/'
    }

    /**
     * 在父路径下添加节点
     * @param parentPath
     * @param node
     * @return
     */
    default T appendTo(String parentPath, T node){
        def paths = acquirePaths(parentPath, 1)
        if(paths.isEmpty()){
            throw new PhysicalPathModelException("$parentPath is full!")
        }
        node.path = paths[0]
        insert(node)
    }

    /**
     * 在父路径下添加节点
     * @param parentPath
     * @param nodes
     * @return
     */
    default <T extends PhysicalPathModel> List<T> appendTo(String parentPath, List<T> nodes){
        def paths = acquirePaths(parentPath, nodes.size())
        nodes.eachWithIndex { T entry, int i ->
            entry.path = paths[i]
            insert(entry)
        }
        nodes
    }

    /**
     * 移除叶子节点
     * @param node
     * @return
     */
    default Integer removeLeafNode(T node) {
        if (!isLeafNode(node.path)) {
            throw new PhysicalPathModelException("${node.path} is not leaf node!")
        }
        deleteTree(node)
    }

    /**
     * 移除子树
     * @param node
     * @return
     */
    default Integer removeTree(T node) {
        deleteTree(node)
    }

    /**
     *
     * @param node
     * @param targetParentPath
     * @return
     */
    default <T extends PhysicalPathModel> T moveTreeTo(T node, String targetParentPath){
        List<T> offspring = queryOffspring(node.path)
        def paths = acquirePaths(targetParentPath, offspring.size())
        offspring.eachWithIndex { T entry, int i ->
            replacePath(entry, paths[i])
        }
    }

    /**
     * 查询子节点（不包含自身）
     * @param path
     * @return
     */
    default List<T> queryChildren(String path) {
        def pathLike = "${path}${pathPrefix()}${'_' * (levelPathLength() - pathPrefix().size())}"
        queryByPathLike(pathLike)
    }

    /**
     * 查询祖先节点（包含自身）
     * @param path
     * @return
     */
    default List<T> queryAncestors(String path) {
        def splitter = pathPrefix()
        List<String> paths = []
        int idx = 0
        while (idx < path.length()) {
            if (path[idx] == splitter) {
                paths << path[0..idx]
            }
            idx++
        }
        queryByPathIn(paths)
    }

    /**
     * 查询后代节点（包含自身）
     * @param path
     * @return
     */
    default List<T> queryOffspring(String path) {
        queryByPathLike("$path%")
    }

    /**
     * 查询子树
     * @param path
     * @return
     */
    default T queryTree(String path){
        def offspring = queryOffspring(path)
        toTree(offspring)
    }

    /**
     * 节点列表转换成树
     * @param nodes
     * @return
     */
    default T toTree(List<T> nodes){
        Map<String,T> nodeMap = [:]
        Map<String,Closure> taskMap = [:]
        T root
        nodes.each{node->
            nodeMap[node.path] = node
            taskMap[node.path] = {
                def parent = parentPath(node.path)
                def parentNode = nodeMap[parent]
                if(parentNode == null){
                    if(root != null){
                        throw new PhysicalPathModelException("unsupported multiple root")
                    }
                    root = node
                }else{
                    if(parentNode.children==null){
                        parentNode.children = []
                    }
                    parentNode.children << node
                }
            }
        }
        taskMap.each {
            it.value()
        }
        root
    }

    /**
     * 计算父节点路径
     * @param path
     * @return
     */
    default String parentPath(String path){
        if(path == null || path.isEmpty()){
            throw new PhysicalPathModelException("invalid path: $path")
        }
        def splitter = pathPrefix()
        if(path == splitter){
            return null
        }
        int idx = path.size()-2
        for(;idx>=0;idx--){
            if(path[idx] == splitter){
                return path[0..idx]
            }
        }
        throw new PhysicalPathModelException("invalid path: $path")
    }
    /**
     * 根路径，必须使用单个字符。
     * @return
     */
    default String rootPath() {
        pathPrefix()
    }

    /**
     * 格式化
     */
    default String toPrettyString(T root){
        def fn
        def idt = "  "
        fn = {node,indent->
            node.children?"${indent}${node.path}\n${node.children?.collect{indent+fn(it,indent+idt)}.join("\n")}":"${indent}${node.path}"
        }
        fn(root,'')
    }


}