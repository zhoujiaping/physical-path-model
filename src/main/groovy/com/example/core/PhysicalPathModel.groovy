package com.example.core

interface PhysicalPathModel<T extends PhysicalPathModel<T>> {
    String getPath()
    void setPath(String path)
    List<T> getChildren()
    void setChildren(List<T> children)
}