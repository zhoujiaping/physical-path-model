package com.example.core

class PhysicalPathModelException extends RuntimeException{
    PhysicalPathModelException(String message) {
        super(message)
    }
}
