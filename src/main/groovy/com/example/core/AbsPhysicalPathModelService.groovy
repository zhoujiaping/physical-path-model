package com.example.core

abstract class AbsPhysicalPathModelService<T extends PhysicalPathModel> implements PhysicalPathModelService<T> {
    /**
     * 删除节点及其后代节点
     * @param path
     * @return
     */
    Integer deleteTree(T node) { throw new PhysicalPathModelException('method not implemented!') }

    /**
     * 根据path模糊查询
     * @param pathLike
     * @return
     */
    List<T> queryByPathLike(String pathLike) { throw new PhysicalPathModelException('method not implemented!') }

    /**
     * 根据path列表查询
     * @param paths
     * @return
     */
    List<T> queryByPathIn(List<String> paths) { throw new PhysicalPathModelException('method not implemented!') }

    /**
     * 根据path模糊查询计数
     * @param pathLike
     * @return
     */
    Integer countByPathLike(String pathLike) { throw new PhysicalPathModelException('method not implemented!') }

    /**
     * 替换path
     * @param oldPath
     * @param newPath
     * @return
     */
    Integer replacePath(T node, String newPath) { throw new PhysicalPathModelException('method not implemented!') }

    /**
     * 新增节点
     * @param node
     * @return
     */
    T insert(T node) { throw new PhysicalPathModelException('method not implemented!') }

    /**
     * 申请节点路径
     * @param parentPath
     * @param count
     * @return
     */
    List<String> acquirePaths(String parentPath, int count) {
        throw new PhysicalPathModelException('method not implemented!')
    }
}
